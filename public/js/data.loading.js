 // Handling dynamic depts selection on region change for dynamically generated data
    function RegionData(regionId)
    {
        var region = $("#"+regionId);
        console.log(region);
        var route = region.data('url');
        route += "/"+region.val();
        //--- On doit déterminer la liste des départements correspondants
        var id=region.attr('id');
        var ids = id.split("_");
        if(ids.length >= 3){
            var thisIndex =  Number(ids[2]);
            var deptId = ids[0]+"_department_"+thisIndex ;
            getSubList(route, deptId,region.data('span') );
        }
    }
    
    // Handling dynamic data selection on dept change for dynamically generated data
    function DeptData(deptId, prefix='district')
    {
        var dept = $("#"+deptId);
        console.log(dept);
        var route = dept.data('url');
        route += "/"+dept.val();
        //--- On doit déterminer la liste des distics correspondants
        var id=dept.attr('id');
        var ids = id.split("_");

        if(ids.length >= 3){
            var thisIndex =  Number(ids[2]);
            var distId = ids[0]+"_"+prefix+"_"+thisIndex ;
            getSubList(route, distId,dept.data('span') );
        }
    }

    function getSubList(completeroutedept, selecttofill, countspan="")
    {
        if( !isEmpty(completeroutedept) && !isEmpty(selecttofill) )
        {
            spin();
            $.ajax({
                url: completeroutedept,
                method: 'GET',
                success: function(retvalues)
                {
                    $("#"+ countspan).empty();

                    // var $none = '@lang('message.message')';
                    $("#"+ selecttofill).empty();
                    $("#"+ selecttofill).append('<option value="" disabled  selected > --- </option>');
                    var count = 0;
                    $.each(retvalues, function(code,valueitem){
                        count++;
                        $("#"+ selecttofill).append('<option value ='+code+'>'+valueitem+'</option>');
                    });
                    $("#"+ countspan).append(' : '+count);
                    unSpin()
                },
                error : (error) =>  {
                    $("#"+ selecttofill).empty();
                    $("#"+ countspan).empty();
                    $("#"+ countspan).append(' : 0');
                    $("#"+ selecttofill).append('<option value="" disabled  selected > ---</option>');
                    unSpin()
                }
            });
        }else
        {
            if( !isEmpty(selecttofill))
            {
                $("#"+ selecttofill).empty();
                $("#"+ countspan).empty();
                $("#"+ countspan).append(' : 0');
                $("#"+ selecttofill).append('<option value="" disabled  selected> --- </option>');
            }
            console.log("Invalid parameters" );
        }
    }

    function isEmpty(str) {
        return (!str || 0 === str.length);
    }

    function spin() {
        $('#spinner').html('<span class="fa fa-spinner fa-pulse fa-3x fa-fw"></span>')

    }

    function unSpin() {
        $('#spinner').empty()

    }
     
