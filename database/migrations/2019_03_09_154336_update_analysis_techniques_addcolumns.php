<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAnalysisTechniquesAddcolumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('analysis_techniques', function (Blueprint $table) {
            $table->string('title_fr')->nullable();
            $table->text('presentation_fr')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('analysis_techniques', function (Blueprint $table) {
            $table->dropColumn('title_fr');
            $table->dropColumn('presentation_fr');
        });
    }
}
