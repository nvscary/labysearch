<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exams', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->nullable();
            $table->string('title');
            $table->text('presentation')->nullable();
            $table->string('coast');
            $table->string('delay');
            $table->string('b')->nullable();
            $table->integer('group_id')->unsigned();
            $table->integer('substance_id')->nullable()->unsigned();
            $table->integer('condition_id')->nullable()->unsigned();
            $table->integer('analysis_technique_id')->nullable()->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('group_id')->references('id')->on('groups')->onDelete('restrict');
            $table->foreign('substance_id')->references('id')->on('substances')->onDelete('restrict');
           
            $table->foreign('condition_id')->references('id')->on('conditions')->onDelete('restrict');
            $table->foreign('analysis_technique_id')->references('id')->on('analysis_techniques')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exams');
    }
}
