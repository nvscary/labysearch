<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAllTablesSetTitlesUnique extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('branches', function (Blueprint $table) {
            $table->string('title_fr')->unique()->change();
            $table->string('title_en')->unique()->change();
        });
        Schema::table('groups', function (Blueprint $table) {
            $table->string('title_fr')->unique()->change();
            $table->string('title_en')->unique()->change();
        });
        Schema::table('exams', function (Blueprint $table) {
            $table->string('title_fr')->unique()->change();
            $table->string('title_en')->unique()->change();
        });
        Schema::table('analysis_techniques', function (Blueprint $table) {
            $table->string('title_fr')->unique()->change();
            $table->string('title_en')->unique()->change();
        });
        Schema::table('substances', function (Blueprint $table) {
            $table->string('title_fr')->unique()->change();
            $table->string('title_en')->unique()->change();
        });
        Schema::table('conditionss', function (Blueprint $table) {
            $table->string('title_fr')->unique()->change();
            $table->string('title_en')->unique()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('branches', function (Blueprint $table) {
            //
        });
    }
}
