<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateGroupsRenamecolumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('groups', function (Blueprint $table) {
           $table->renameColumn('title', 'title_en');
           $table->renameColumn('presentation', 'presentation_en');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('groups', function (Blueprint $table) {
           $table->renameColumn('title_en', 'title');
           $table->renameColumn('presentation_en', 'presentation');
        });
    }
}
