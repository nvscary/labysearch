<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBranchesRenamecolumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('branches', function (Blueprint $table) {
           $table->renameColumn('title', 'title_en');
           $table->renameColumn('presentation', 'presentation_en');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('branches', function (Blueprint $table) {
           $table->renameColumn('title_en', 'title');
           $table->renameColumn('presentation_en', 'presentation');
        });
    }
}
