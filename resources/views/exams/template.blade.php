      
@if (session('status'))
<div class="alert alert-success" role="alert">
    {{ session('status') }}
</div>
@endif
      
@if (session('error'))
<div class="alert alert-danger" role="alert">
    {{ session('error') }}
</div>
@endif
<?php
    if ($exam->id) {
        $options = ['method' => 'put', 'id'=>'examForm', 'url' => route('exams.update', $exam) ];
    } else {
        $options = [ 'method' => 'post','id'=>'examForm', 'url' => route('exams.store')];
    }
?>
{!! Form::model($exam, $options) !!}
{{ csrf_field() }} 
    <div class="form-group row">
        <label for="code" class="col-md-4 col-form-label text-md-right">@lang('exams.code')</label>

        <div class="col-md-6">
            <input id="code" type="text" class="form-control{{ $errors->has('code') ? ' is-invalid' : '' }}" name="code" value="{{ $exam->code}}" >

            @if ($errors->has('code'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('code') }}</strong>
                </span>
            @endif
        </div>
    </div> 
    <div class="form-group row">
        <label for="title_fr" class="col-md-4 col-form-label text-md-right">@lang('exams.title_fr')</label>

        <div class="col-md-6">
            <input id="title_fr" type="text" class="form-control{{ $errors->has('title_fr') ? ' is-invalid' : '' }}" name="title_fr" value="{{ $exam->title_fr}}" required autofocus>

            @if ($errors->has('title_fr'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('title_fr') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="title_en" class="col-md-4 col-form-label text-md-right">@lang('exams.title_en')</label>

        <div class="col-md-6">
            <input id="title_en" type="text" class="form-control{{ $errors->has('title_en') ? ' is-invalid' : '' }}" name="title_en" value="{{ $exam->title_en}}" required autofocus>

            @if ($errors->has('title_en'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('title_en') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="b" class="col-md-4 col-form-label text-md-right">@lang('exams.b')</label>

        <div class="col-md-6">
            <input id="b" type="text" class="form-control{{ $errors->has('b') ? ' is-invalid' : '' }}" name="b" value="{{ $exam->b}}" >

            @if ($errors->has('b'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('b') }}</strong>
                </span>
            @endif
        </div>
    </div> 
    <div class="form-group row">
        <label for="coast" class="col-md-4 col-form-label text-md-right">@lang('exams.coast')</label>

        <div class="col-md-6">
            <input id="coast" type="text" class="form-control{{ $errors->has('coast') ? ' is-invalid' : '' }}" name="coast" value="{{ $exam->coast}}" >

            @if ($errors->has('coast'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('coast') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="delay" class="col-md-4 col-form-label text-md-right">@lang('exams.delay')</label>

        <div class="col-md-6">
            <input id="delay" type="text" class="form-control{{ $errors->has('delay') ? ' is-invalid' : '' }}" name="delay" value="{{ $exam->delay}}" >

            @if ($errors->has('delay'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('delay') }}</strong>
                </span>
            @endif
        </div>
    </div> 
    <div class="form-group {{ $errors->has('branch_id') ? 'has-error' : '' }} row">
       <label for="b" class="col-md-4 col-form-label text-md-right">@lang('exams.branch_id')</label>
       <div class="col-md-6">
            {!!  Form::select('branch_id', 
                          $branches, 
                          null, 
                          [ 
                            'class'=>'form-control', 
                            'id'=> 'branch_id', 
                            'onchange' => "getSubList('".route('branches.groups' )."/'+this.value, 'group_id','group_count')",
                            'required' => true,
                            ]) 
            !!}
            {!! $errors->first('branch_id', '<span class="invalid-feedback">:message</span>') !!}
        </div>
    </div>

    <div class="form-group {{ $errors->has('group_id') ? 'has-error' : '' }} row">
        <label for="b" class="col-md-4 col-form-label text-md-right">@lang('exams.group_id')<span style="color:red" id="group_count"></span></label>
            <div class="col-md-6">
            {!!  Form::select('group_id', 
                          $groups, 
                          null, 
                          [ 
                            'class'=>'form-control', 
                            'data-style'=> 'white', 
                            'id'=> 'group_id', 
                            'required' => true,
                            ]) 
            !!}
            {!! $errors->first('group_id', '<span class="invalid-feedback">:message</span>') !!}
        </div>
    </div>
    <div class="form-group row">
        <label for="substance_id" class="col-md-4 col-form-label text-md-right">@lang('exams.substance_id')</label>

        <div class="col-md-6">
            {!!  Form::select('substance_id', 
                          $substances, 
                          null, 
                          [ 
                            'class'=>'form-control', 
                            'data-style'=> 'white', 
                            'id'=> 'substance_id', 
                            'required' => true,
                            ]) 
            !!}
            @if ($errors->has('substance_id'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('substance_id') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="analysis_technique_id" class="col-md-4 col-form-label text-md-right">@lang('exams.analysis_technique_id')</label>

        <div class="col-md-6">
            {!!  Form::select('analysis_technique_id', 
                          $analysis_techniques, 
                          null, 
                          [ 
                            'class'=>'form-control', 
                            'data-style'=> 'white', 
                            'id'=> 'analysis_technique_id', 
                            'required' => true,
                            ]) 
            !!}
            @if ($errors->has('analysis_technique_id'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('analysis_technique_id') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="condition_id" class="col-md-4 col-form-label text-md-right">@lang('exams.conditions')</label>

        <div class="col-md-6">
            {!!  Form::select('condition_id[]', 
                          $conditions, 
                          null, 
                          [ 
                            'class'=>'form-control', 
                            'data-style'=> 'white', 
                            'id'=> 'condition_id', 
                            ]) 
            !!}
            @if ($errors->has('condition_id'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('condition_id') }}</strong>
                </span>
            @endif
        </div>
    </div>
<div class="form-group row">
    <label for="presentation_fr" class="col-md-4 col-form-label text-md-right">@lang('exams.presentation_fr')</label>

    <div class="col-md-6">
        <textarea id="presentation_fr" rows="3" class="form-control{{ $errors->has('presentation_fr') ? ' is-invalid' : '' }}" name="presentation_fr"  value="{{ $exam->presentation_fr}}" > </textarea> 

        @if ($errors->has('presentation_fr'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('presentation_fr') }}</strong>
            </span>
        @endif
    </div>
</div>
 
<div class="form-group row">
    <label for="presentation_en" class="col-md-4 col-form-label text-md-right">@lang('exams.presentation_en')</label>

    <div class="col-md-6">
        <textarea id="presentation_en" rows="3" class="form-control{{ $errors->has('presentation_en') ? ' is-invalid' : '' }}" name="presentation_en"  value="{{ $exam->presentation_en}}" > </textarea> 

        @if ($errors->has('presentation_en'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('presentation_en') }}</strong>
            </span>
        @endif
    </div>
</div>
 
    <div class="form-group row mb-0">
        <div class="col-md-6 offset-md-4">
            <button type="submit" class="btn btn-primary">
                @lang('buttons.submit')
            </button>
        </div>
    </div>

{!! Form::close()!!} 
