@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">@lang('exams.edit')</div>

                <div class="card-body">
                        @include('exams.template', compact('exam'))
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script src="{{ asset('js/data.loading.js') }}"></script>
@endsection

