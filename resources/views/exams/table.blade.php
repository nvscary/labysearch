@php
$title = trans('message.title');
@endphp
@forelse($exams as $exam)
    <tr id="elem-{{ $exam->id }}" >
        <td ><a href="{{ route('exams.show', [$exam->id]) }}" >{{ $exam->title_fr }}</a></td>
        <td > {{ $exam->title_en}} </td>
        <td>{{ $exam->branch->$title }}</td>
        <td>{{ $exam->group->$title }}</td>
        <td>{{ $exam->b}}</td>
        <td>{{ $exam->coast}}</td>
        <td>{{ $exam->delay}}</td>
        <td><a href="{{ route('exams.show', [$exam->id]) }}" class="btn btn-success" >@lang('buttons.details')</a></td>
        <td>
            <a href="{{ route('exams.edit', [$exam->id]) }}" class="btn btn-warning"> @lang('buttons.edit') </a>
        </td>   
        <td>
            {!! Form::open(['method' => 'delete', 'url' => route('exams.destroy', $exam), 'class' => 'delete', 'data-id' => $exam->id]) !!}
                <button type="submit" class="btn btn-danger btn-xs pull-right"><i class="fa fa-trash"></i>  @lang('buttons.delete')</button>
            {!! Form::close() !!}

        </td>
    </tr>
@empty
 <tr>
    <p class="text-center margin-half">
        @lang('message.notfoundrecords')
    </p>
</tr>
@endforelse
