@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">@lang('branches.show')</div>

                <div class="card-body">
                   <p>
                    @lang('branches.title_fr') : {{ $branch->title_fr}} <br/>
                     @lang('branches.title_en') : {{ $branch->title_en}} <br/>
                     @lang('branches.groups_count') : 
                     @if(count($branch->groups))
                      <a href="{{ route('groups.index',['branch_id'=>$branch->id])}}"> {{ count($branch->groups)}}</a> 
                       @else 0
                       @endif

                      <br/>
                     @lang('exams.exams_count') : 
                     @if(count($branch->exams))
                      <a href="{{ route('exams.index',['branch_id'=>$branch->id])}}">  {{ count($branch->exams)}} </a>
                       @else 0
                       @endif<br/>
                   </p>
                   @lang('branches.presentation_fr') : <p> {{ $branch->presentation_fr}}</p>
                   @lang('branches.presentation_en') : <p>{{ $branch->presentation_en}}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
