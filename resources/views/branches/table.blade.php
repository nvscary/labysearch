@forelse($branches as $branch)
    <tr id="elem-{{ $branch->id }}" >
        <td ><a href="{{ route('branches.show', [$branch->id]) }}" >{{ $branch->title_fr }}</a></td>
        <td > {{ $branch->title_en}} </td>
        <td><a href="{{ route('branches.show', [$branch->id]) }}" class="btn btn-success" >@lang('buttons.details')</a></td>
        <td>
            <a href="{{ route('branches.edit', [$branch->id]) }}" class="btn btn-warning"> @lang('buttons.edit') </a>
        </td>   
        <td>
            {!! Form::open(['method' => 'delete', 'url' => route('branches.destroy', $branch), 'class' => 'delete', 'data-id' => $branch->id]) !!}
                <button type="submit" class="btn btn-danger btn-xs pull-right"><i class="fa fa-trash"></i>  @lang('buttons.delete')</button>
            {!! Form::close() !!}
        </td>
    </tr>
@empty
 <tr>
    <p class="text-center margin-half">
        @lang('message.notfoundrecords')
    </p>
</tr>
@endforelse
