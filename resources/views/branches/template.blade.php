      
@if (session('status'))
<div class="alert alert-success" role="alert">
    {{ session('status') }}
</div>
@endif
      
@if (session('error'))
<div class="alert alert-danger" role="alert">
    {{ session('error') }}
</div>
@endif

<?php
    if ($branch->id) {
        $options = ['method' => 'put', 'id'=>'examForm', 'url' => route('branches.update', $branch) ];
    } else {
        $options = [ 'method' => 'post','id'=>'examForm', 'url' => route('branches.store')];
    }
?>
{!! Form::model($branch, $options) !!}
{{ csrf_field() }} 
<div class="form-group row">
    <label for="title_fr" class="col-md-4 col-form-label text-md-right">@lang('branches.title_fr')</label>

    <div class="col-md-6">
        <input id="title_fr" type="text" class="form-control{{ $errors->has('title_fr') ? ' is-invalid' : '' }}" name="title_fr" value="{{ $branch->title_fr}}" required autofocus>

        @if ($errors->has('title_fr'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('title_fr') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label for="title_en" class="col-md-4 col-form-label text-md-right">@lang('branches.title_en')</label>

    <div class="col-md-6">
        <input id="title_en" type="text" class="form-control{{ $errors->has('title_en') ? ' is-invalid' : '' }}" name="title_en" value="{{ $branch->title_en}}" required autofocus>

        @if ($errors->has('title_en'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('title_en') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label for="presentation_fr" class="col-md-4 col-form-label text-md-right">@lang('branches.presentation_fr')</label>

    <div class="col-md-6">
        <textarea id="presentation_fr" rows="3" class="form-control{{ $errors->has('presentation_fr') ? ' is-invalid' : '' }}" name="presentation_fr"  value="{{ $branch->presentation_fr}}" > </textarea> 

        @if ($errors->has('presentation_fr'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('presentation_fr') }}</strong>
            </span>
        @endif
    </div>
</div>
 
<div class="form-group row">
    <label for="presentation_en" class="col-md-4 col-form-label text-md-right">@lang('branches.presentation_en')</label>

    <div class="col-md-6">
        <textarea id="presentation_en" rows="3" class="form-control{{ $errors->has('presentation_en') ? ' is-invalid' : '' }}" name="presentation_en"  value="{{ $branch->presentation_en}}" > </textarea> 

        @if ($errors->has('presentation_en'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('presentation_en') }}</strong>
            </span>
        @endif
    </div>
</div>
 
<div class="form-group row mb-0">
    <div class="col-md-6 offset-md-4">
        <button type="submit" class="btn btn-primary">
            @lang('buttons.submit')
        </button>
    </div>
</div>
 
{!! Form::close()!!}             