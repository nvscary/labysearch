@extends('layouts.app')
@php
$title = trans('message.title');
@endphp
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">@lang('groups.show') </div>

                <div class="card-body">
                   @lang('groups.title_fr') : {{ $group->title_fr}} <br/>
                   @lang('groups.title_en') : {{ $group->title_en}} <br/>

                     @lang('exams.exams_count') : <a href="{{ route('exams.index',['group_id'=>$group->id])}}">  {{ count($group->exams)}} </a><br/>

                   @lang('groups.branch_id') : {{ $group->branch->$title}} <br/>
                   @lang('groups.presentation_fr'): {{ $group->presentation_fr}}
                   @lang('groups.presentation_en'): {{ $group->presentation_en}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
