@php
$title = trans('message.title');
@endphp
@forelse($groups as $group)
    <tr id="elem-{{ $group->id }}" >
        <td ><a href="{{ route('groups.show', [$group->id]) }}" >{{ $group->title_fr }}</a></td>
        <td > {{ $group->title_en}} </td>
        <td>{{ $group->branch->$title}}</td>
        <td><a href="{{ route('groups.show', [$group->id]) }}" class="btn btn-success" >@lang('buttons.details')</a></td>
        <td>
            <a href="{{ route('groups.edit', [$group->id]) }}" class="btn btn-warning"> @lang('buttons.edit') </a>
        </td> 
        <td>
            {!! Form::open(['method' => 'delete', 'url' => route('groups.destroy', $group), 'class' => 'delete', 'data-id' => $group->id]) !!}
                <button type="submit" class="btn btn-danger btn-xs pull-right"><i class="fa fa-trash"></i>  @lang('buttons.delete')</button>
            {!! Form::close() !!}

        </td>
    </tr>
@empty
 <tr>
    <p class="text-center margin-half">
        @lang('message.notfoundrecords')
    </p>
</tr>
@endforelse
