@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">@lang('analysis_techniques.show')</div>

                <div class="card-body">
                   @lang('analysis_techniques.code') : {{ $analysis_technique->code}} <br/> 
                   @lang('analysis_techniques.title_fr') : {{ $analysis_technique->title_fr}} <br/> 
                   @lang('analysis_techniques.title_en') : {{ $analysis_technique->title_en}} <br/> 
                   @lang('analysis_techniques.presentation_fr') : {{ $analysis_technique->presentation_fr}} <br/> 
                   @lang('analysis_techniques.presentation_en') : {{ $analysis_technique->presentation_en}} <br/> 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
