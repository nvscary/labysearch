@forelse($analysis_techniques as $analysis_technique)
    <tr id="elem-{{ $analysis_technique->id }}" >
        <td ><a href="{{ route('analysis_techniques.show', [$analysis_technique->id]) }}" >{{ $analysis_technique->title_fr }}</a></td>
        <td > {{ $analysis_technique->title_en}} </td>
        <td>{{$analysis_technique->code}}</td>
        <td><a href="{{ route('analysis_techniques.show', [$analysis_technique->id]) }}" class="btn btn-success" >@lang('buttons.details')</a></td>
        <td>
            <a href="{{ route('analysis_techniques.edit', [$analysis_technique->id]) }}" class="btn btn-warning"> @lang('buttons.edit') </a>
        </td>  
        <td>
            {!! Form::open(['method' => 'delete', 'url' => route('analysis_techniques.destroy', $analysis_technique), 'class' => 'delete', 'data-id' => $analysis_technique->id]) !!}
                <button type="submit" class="btn btn-danger btn-xs pull-right"><i class="fa fa-trash"></i>  @lang('buttons.delete')</button>
            {!! Form::close() !!}
        </td>
    </tr>
@empty
 <tr>
    <p class="text-center margin-half">
        @lang('message.notfoundrecords')
    </p>
</tr>
@endforelse
