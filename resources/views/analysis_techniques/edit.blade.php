@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">@lang('analysis_techniques.edit')</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('analysis_techniques.update', [$analysis_technique->id]) }}">
                        {{ method_field('PUT') }}
                        @csrf

                        @include('analysis_techniques.template', compact('analysis_technique'))
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
