@extends('layout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">@lang('exams.list')</div>
                <div class="card-body">
                    <!--<form id='search_form'>-->
                        <div class="form-group row">
                            <div class="col-md-10">
                                <input id="search" type="text" class="form-control" name="search" placeholder="@lang('message.search_word')">
                            </div>
                            <div class="col-md-2">
                                <button class="btn btn-primary" id="searchSubmit">
                                    @lang('buttons.search')
                                </button>
                            </div>
                        </div>
                    <!--</form>-->
                    
                    <div class="box-body table-responsive">
                        <table id="example" class="table table-bordered">
                           <thead>
                                <tr>
                                    <th>@lang('exams.title') </th>
                                    <th>@lang("exams.branch_id") </th>
                                    <th>@lang("exams.branch_id") </th>
                                    <th>@lang("exams.b") </th>
                                    <th>@lang("exams.coast") </th>
                                    <th>@lang("exams.delay") </th>
                                    <th>@lang('exams.analysis_technique_id')</th>
                                    <th>@lang('exams.substance_id')</th>

                                </tr>
                            </thead>
                            <tbody id="pannel">
                                @include('front.exams.table', compact('exams'))
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div id="pagination" class="box-footer">
                        {{ $links }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script') 
<!-- jQuery 3.2.0 -->
<script src="{{ asset('js/jquery-3.2.0.min.js') }}"></script> 
<script src="//cdn.jsdelivr.net/sweetalert2/6.3.8/sweetalert2.min.js"></script>
<script type= "text/javascript">
    $("body").on("click","#searchSubmit",function(e)
    {
        e.preventDefault(); 
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.ajax({
            url: "{{ route('front.search') }} ",
            method: 'POST',
            data:{search:$('#search').val()},
            success: function(exams ) {

               $("tbody").empty();
                if(exams.length == 0)
                {

                  var $notfound = "@lang('message.notfoundrecords')";
                      // alert($title);
                 var body =  '<tr><td colspan="8" class="text-center">'+$notfound +'</td></tr>';
                 $('tbody').append(body);
                }else
                {
                      var $title = '@lang('message.title')';
                      var $none = '@lang('message.none')';
                   $.each(exams, function(i, exam)
                    {
                      // alert($title);
                      console.log(exam);
                      console.log($title);
                      body = '<tr id="elem-'+exam.id+'">';
                      var url = '/front/exams/'+exam.id;
                      body    += '<td class="text-primary"> <a href="'+url+'">'+exam.title_fr+' </a> '   + '</td>';
                      body    += '<td>'+exam.code + '</td>';
                      body    += '<td>'+exam.branch.title_fr + '</td>';
                      body    += '<td>'+exam.group.title_fr + '</td>';
                      body    += '<td>'+exam.b + '</td>';
                      body    += '<td>'+exam.coast + '</td>';
                      body    += '<td>'+exam.delay + '</td>';
                      body    += '<td>' ;
                      if(exam.analysis_technique){
                        body    += exam.analysis_technique.title_fr + '</td>';
                      }else 
                        body    += $none+'</td>';
                        
                      body    += '<td>' ;
                      if(exam.substance){
                        body    += exam.substance.title_fr + '</td>';
                      }else 
                        body    += $none+'</td>';
                      
                      body    += "</tr>";
                      // console.log(body);
                      $('tbody').append(body);
                    });
                }
            }
        });
      });
  </script>

@endsection
