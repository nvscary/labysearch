@extends('layout')
@php
$title = trans('message.title');
$presentation = trans('message.presentation');
@endphp
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            
            <div class="card">
                <div class="card-header">@lang('exams.show') </div>
                  <div class="card-body">
                     @lang('exams.title') : {{ $exam->$title}} <br/>
                     @lang('exams.code') : {{ $exam->code}} <br/>
                     @lang('exams.b') : {{ $exam->b}} <br/>
                     @lang('exams.coast') : {{ $exam->coast}} <br/>
                     @lang('exams.delay') : {{ $exam->delay}} <br/>
                     @lang('exams.branch_id') : {{ $exam->branch->$title}} <br/>
                     @lang('exams.group_id') : {{ $exam->group->$title}} <br/>
                     @lang('exams.analysis_technique_id') : {{ $exam->analysis_technique ?   $exam->analysis_technique->$title : trans('message.none')}} <br/>
                     @lang('exams.substance_id') : {{ $exam->substance ?  $exam->substance->$title : trans('message.none') }} <br/>
                     @lang('exams.conditions') :   <br/>

                    @if(!is_null( $exam->conditions))
                    <ul>
                        @foreach($exam->conditions as $condition)
                            @if($condition)<li>{{$condition->$title}}</li>@endif
                        @endforeach
                    </ul>
                    @else
                        @lang('message.none')
                    @endif

                      <h4> @lang('exams.presentation') : </h4> <br/>
                     <p>
                       {{ $exam->$presentation}}
                     </p>
                     
                  </div>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
