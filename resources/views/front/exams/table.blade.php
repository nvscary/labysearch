
@php
$title = trans('message.title');
@endphp
@forelse($exams as $exam)
    <tr id="elem-{{ $exam->id }}" >
         <td ><a href="{{ route('front.exam', [$exam->id]) }}" >{{ $exam->$title }}</a></td>
        <td>{{ $exam->code }} </td>
        <td>{{ $exam->branch->$title}}</td>
        <td>{{ $exam->group->$title}}</td>
        <td>{{ $exam->b}}</td>
        <td>{{ $exam->coast}}</td>
        <td>{{ $exam->delay}}</td>
        <td>{{ $exam->analysis_technique ?   $exam->analysis_technique->$title : trans('message.none')}}</td>
        <td>{{ $exam->substance ?  $exam->substance->$title : trans('message.none')}} </td>
    </tr>
@empty
    <tr>
    <p class="text-center margin-half">
        @lang('message.notfoundrecords')
    </p>
</tr>
@endforelse