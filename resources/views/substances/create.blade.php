@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">@lang('substances.create')</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('substances.store') }}">
                        @csrf

                        @include('substances.template', compact('substance'))
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
