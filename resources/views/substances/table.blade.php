@forelse($substances as $substance)
    <tr id="elem-{{ $substance->id }}" >
        <td ><a href="{{ route('substances.show', [$substance->id]) }}" >{{ $substance->title_fr }}</a></td>
        <td > {{ $substance->title_en}} </td>
        <td>{{$substance->code}}</td>
        <td><a href="{{ route('substances.show', [$substance->id]) }}" class="btn btn-success" >@lang('buttons.details')</a></td>
        <td>
            <a href="{{ route('substances.edit', [$substance->id]) }}" class="btn btn-warning"> @lang('buttons.edit') </a>
        </td> 
        <td>
            {!! Form::open(['method' => 'delete', 'url' => route('substances.destroy', $substance), 'class' => 'delete', 'data-id' => $substance->id]) !!}
                <button type="submit" class="btn btn-danger btn-xs pull-right"><i class="fa fa-trash"></i>  @lang('buttons.delete')</button>
            {!! Form::close() !!}

        </td>
    </tr>
@empty
 <tr>
    <p class="text-center margin-half">
        @lang('message.notfoundrecords')
    </p>
</tr>
@endforelse
