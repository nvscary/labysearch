@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">@lang('substances.show')</div>

                <div class="card-body">
                   @lang('substances.title') : {{ $substance->title}} <br/>
                   @lang('substances.code') : {{ $substance->code}} <br/>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
