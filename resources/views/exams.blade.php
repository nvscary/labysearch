@extends('layout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Liste des examens</div>

                <div class="card-body">
                    
                    <div class="form-group row">
                        <div class="col-md-10">
                            <input id="search" type="text" class="form-control" name="search" placeholder="Recherche">
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Valider') }}
                            </button>
                        </div>
                    </div>
                    <div class="box-body table-responsive">
                        <table id="example" class="table table-bordered">
                           <thead>
                                <tr>
                                    <th>@lang('Intitulé') </th>
                                    <th>@lang('Code') </th>
                                    <th>@lang("Branche d'exercice") </th>
                                    <th>@lang("Groupe d'examens") </th>
                                    <th>@lang("B") </th>
                                    <th>@lang("Coût en FCFA") </th>
                                    <th>@lang("Délais") </th>
                                    <th>@lang("Type de RDV à prendre") </th>
                                    <th>@lang("Technique d'analyse") </th>
                                    <th> @lang("Technique de prélèvement ")</th>
                                    <th> @lang("Condition de prélèvement")</th>
                                </tr>
                            </thead>
                            <tbody id="pannel">
                                @forelse($exams as $exam)
                                    <tr id="elem-{{ $exam->id }}" >
                                        <td>{{ $exam->title }} </td>
                                        <td>{{ $exam->code }} </td>
                                        <td>{{ $exam->branch->title}}</td>
                                        <td>{{ $exam->group->title}}</td>
                                        <td>{{ $exam->b}}</td>
                                        <td>{{ $exam->coast}}</td>
                                        <td>{{ $exam->delay}}</td>
                                        <td>{{ $exam->appointment_type ?   $exam->appointment_type->title : 'Aucun'}}</td>
                                        <td>{{ $exam->analysis_technique ?   $exam->analysis_technique->title : 'Aucun'}}</td>
                                        <td>{{ $exam->sampling_technique ?  $exam->sampling_technique->title : 'Aucun'}} </td>
                                        <td>{{ $exam->sampling_condition ? $exam->sampling_condition->title : 'Aucun'}}</td>
                                    </tr>
                                @empty
                                 <tr>
                                    <p class="text-center margin-half">
                                        @lang('message.notfoundrecords')
                                    </p>
                                </tr>
                                @endforelse

                            </tbody>
                        </table>
                    </div>

                        
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
@endsection
