@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">@lang('conditions.edit')</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('conditions.update', [$condition->id]) }}">
                    	{{ method_field('PUT') }}
                        @csrf

                        @include('conditions.template', compact('condition'))
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
