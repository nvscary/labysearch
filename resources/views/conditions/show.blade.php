@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">@lang('conditions.show')</div>

                <div class="card-body">
                   @lang('conditions.title_fr') : {{ $condition->title_fr}} <br/> 
                   @lang('conditions.title_en') : {{ $condition->title_en}} <br/> 
                   @lang('conditions.code') : {{ $condition->code}} <br/> 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
