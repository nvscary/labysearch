@forelse($conditions as $condition)
    <tr id="elem-{{ $condition->id }}" >
        <td ><a href="{{ route('conditions.show', [$condition->id]) }}" >{{ $condition->title_fr }}</a></td>
        <td > {{ $condition->title_en}} </td>
        <td>{{$condition->code}}</td>
        <td><a href="{{ route('conditions.show', [$condition->id]) }}" class="btn btn-success" >@lang('buttons.details')</a></td>
        <td>
            <a href="{{ route('conditions.edit', [$condition->id]) }}" class="btn btn-warning"> @lang('buttons.edit') </a>
        </td> 
        <td>
            {!! Form::open(['method' => 'delete', 'url' => route('conditions.destroy', $condition), 'class' => 'delete', 'data-id' => $condition->id]) !!}
                <button type="submit" class="btn btn-danger btn-xs pull-right"><i class="fa fa-trash"></i>  @lang('buttons.delete')</button>
            {!! Form::close() !!}
        </td>
    </tr>
@empty
 <tr>
    <p class="text-center margin-half">
        @lang('message.notfoundrecords')
    </p>
</tr>
@endforelse
