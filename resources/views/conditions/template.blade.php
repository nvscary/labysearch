      
@if (session('status'))
<div class="alert alert-success" role="alert">
    {{ session('status') }}
</div>
@endif
      
@if (session('error'))
<div class="alert alert-danger" role="alert">
    {{ session('error') }}
</div>
@endif

<div class="form-group row">
    <label for="code" class="col-md-4 col-form-label text-md-right">@lang('conditions.code')</label>

    <div class="col-md-6">
        <input id="code" type="text" class="form-control{{ $errors->has('code') ? ' is-invalid' : '' }}" name="code" value="{{ $condition->code}}" required autofocus>

        @if ($errors->has('code'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('code') }}</strong>
            </span>
        @endif
    </div>
</div> 
<div class="form-group row">
    <label for="title_fr" class="col-md-4 col-form-label text-md-right">@lang('conditions.title_fr')</label>
    <div class="col-md-6">
        <textarea id="title_fr" rows=3 class="form-control{{ $errors->has('title_fr') ? ' is-invalid' : '' }}" name="title_fr" value="{{ $condition->title_fr}}" required autofocus></textarea> 

        @if ($errors->has('title_fr'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('title_fr') }}</strong>
            </span>
        @endif
    </div>
</div> 
<div class="form-group row">
    <label for="title_en" class="col-md-4 col-form-label text-md-right">@lang('conditions.title_en')</label>
    <div class="col-md-6">
        <textarea id="title_en" rows=3 class="form-control{{ $errors->has('title_en') ? ' is-invalid' : '' }}" name="title_en" value="{{ $condition->title_en}}" required autofocus></textarea> 

        @if ($errors->has('title_en'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('title_en') }}</strong>
            </span>
        @endif
    </div>
</div> 
<div class="form-group row mb-0">
    <div class="col-md-6 offset-md-4">
        <button type="submit" class="btn btn-primary">
            @lang('buttons.submit')
        </button>
    </div>
</div>
             