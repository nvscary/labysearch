@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">@lang('conditions.list')</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-info" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    @if (session('success'))
                    <div class="alert alert-success" role="alert">
                        {{ session('success') }}
                    </div>
                    @endif
                    @if (session('error'))
                    <div class="alert alert-danger" role="alert">
                        {{ session('error') }}
                    </div>
                    @endif
                    @if (session('warning'))
                    <div class="alert alert-warning" role="alert">
                        {{ session('warning') }}
                    </div>
                    @endif

                    <div class="box-body table-responsive">
                        <table id="example" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>@lang('conditions.title_fr') </th>
                                    <th>@lang('conditions.title_en') </th>
                                    <th>@lang('conditions.code')</th>
                                    <th>@lang('buttons.details')</th>
                                    <th>@lang('buttons.edit')</th>
                                    <th>@lang('buttons.delete')</th>
                                </tr>
                            </thead>
                            <tbody id="pannel">
                                @include('conditions.table', compact('conditions'))
                            </tbody>
                        </table>
                    </div>

                        
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
