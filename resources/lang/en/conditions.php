<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Conditions Language Lines
    |--------------------------------------------------------------------------
    |
    |
    */

    'code' =>'Code' , 
    'title_en' =>'Condition designation - English' , 
    'title_fr' =>'Condition designation - French' , 
    'presentation_en' =>'Condition description/presentation - English' ,
    'presentation_fr' =>'Condition description/presentation - French' ,
    'create' =>'Create a new condition' ,
    'list' =>'List of conditions' ,
    'show' =>'Details of a condition' ,
    'edit' =>'Edit a condition' ,

    'created' =>'Condition created !' ,
    'updated' =>'Condition updated !' ,
    'deleted' =>'Condition deleted !' ,
];
