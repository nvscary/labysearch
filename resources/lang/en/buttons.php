<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Buttons Language Lines
    |--------------------------------------------------------------------------
    |
    |
    */

    'submit' => 'Submit',
    'cancel' => 'Cancel',
    'details' => 'Details',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'search' => 'Search',

];
