<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Analysis techniques Language Lines
    |--------------------------------------------------------------------------
    |
    |
    */

    'code' =>'Code' , 
    'title_en' =>'Technique designation - English' , 
    'title_fr' =>'Technique designation - French' , 
    'presentation_en' =>'Technique description - English' ,
    'presentation_fr' =>'Technique description - French' ,
    'create' =>'Create a new technique' ,
    'list' =>'List of analsis techniques' ,
    'show' =>'Details of analysis technique' ,
    'edit' =>'Edit an analysis technique' ,

    'created' =>'Technique created !' ,
    'updated' =>'Technique updated !' ,
    'deleted' =>'Technique deleted !' ,
];
