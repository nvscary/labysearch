<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Exam groups Language Lines
    |--------------------------------------------------------------------------
    |
    |
    */

    'code' =>'Code' , 
    'title_en' =>'Group designation - English' , 
    'title_fr' =>'Group designation - French' , 
    'presentation_en' =>'Group description - English' ,
    'presentation_fr' =>'Group description - French' ,
    'branch_id' =>'Exam branch' , 
	'create' =>'Create a new group' ,
    'list' =>'List of exam groups' ,
    'show' =>'Details of a group' ,
    'edit' =>'Edit an exam group' ,

    'created' =>'Group created !' ,
    'updated' =>'Group updated !' ,
    'deleted' =>'Group deleted !' ,
    'exams_count' =>'Exams count' , 
];
