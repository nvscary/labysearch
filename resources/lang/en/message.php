<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Message Language Lines
    |--------------------------------------------------------------------------
    |
    |
    */

    'notfoundrecords' => 'No record found', 
    'none' => 'None ', 
    'choose' => 'Choose', 
    'search_word'    => 'Enter a search keyword',
    'save_error' => 'An error occured while saving. Please check your infos and retry.', 
    'title'    => 'title_en',
    'presentation'    => 'presentation_en',
    'cantdelete' =>'Sorry, you can\'t delete() this item !' ,

];
