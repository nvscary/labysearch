<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Substances Language Lines
    |--------------------------------------------------------------------------
    |
    |
    */
    'code' =>'Code' , 
    'title_en' =>'Substance designation - English' , 
    'title_fr' =>'Substance designation - French' , 
    'presentation_en' =>'Substance description - English' ,
    'presentation_fr' =>'Substance description - French' ,
	'create' =>'Create a new group' ,
    'list' =>'List of substances to be analyzed' ,
    'show' =>'Details of a substance' ,
    'edit' =>'Edit a substance' ,

    'created' =>'Substance created !' ,
    'updated' =>'Substance updated !' ,
    'deleted' =>'Substance deleted !' ,
];
