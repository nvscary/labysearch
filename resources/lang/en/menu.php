<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menu Language Lines
    |--------------------------------------------------------------------------
    |
    |
    */

    'login' => 'Log in',
    'logout' => 'Log out',
    'register' => 'Register',
    'list' => 'List',
    'add' => 'Add',
    'dashboard' => 'Dashboard',
    'exams' => 'Exams',
    'groups' => 'Exam groups',
    'branches' => 'Branches',
    'analysis_techniques' => 'Analytical techniques',
    'conditions' => 'Conditions',
    'substances' => 'Substances analyzed',

];
