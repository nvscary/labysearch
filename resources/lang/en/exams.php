<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Exams Language Lines
    |--------------------------------------------------------------------------
    |
    |
    */

    'code' =>'Code' , 
    'b' =>'B' , 
    'title' =>'Exam designation' , 
    'title_en' =>'Exam designation - English' , 
    'title_fr' =>'Exam designation - French' , 
    'presentation_en' =>'Exam description - English' ,
    'presentation_fr' =>'Exam description - French' ,
    'coast' =>'Coast in CFA' , 
    'delay' =>'Delay' , 
    'branch_id' =>'Exam branch' , 
    'group_id' =>'Exam group' , 
    'analysis_technique_id' =>'Analytical technique' , 
    'substance_id' =>'Substance analyzed' , 
    'conditions' =>'Examination conditions' , 

    'create' =>'Create a new exam' ,
    'list' =>'List of exams' ,
    'show' =>'Details of an exam' ,
    'edit' =>'Edit an exam' ,

    'created' =>'Exam created !' ,
    'updated' =>'Exam updated !' ,
    'deleted' =>'Exam deleted !' ,
    'exams_count' =>'Exams count' , 
    
];
