<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Branches Language Lines
    |--------------------------------------------------------------------------
    |
    |
    */

    'code' =>'Code' , 
    'title_en' =>'Branch designation - English' , 
    'title_fr' =>'Branch designation - French' , 
    'presentation_en' =>'Branch description - English' ,
    'presentation_fr' =>'Branch description - French' ,
    'create' =>'Create a new branch' ,
    'list' =>'List of branches' ,
    'show' =>'Details of analysis branch' ,
    'edit' =>'Edit an analysis branch' ,

    'created' =>'Branch created !' ,
    'updated' =>'Branch updated !' ,
    'deleted' =>'Branch deleted !' ,

    'groups_count' =>'Groups count' , 
    'exams_count' =>'Exams count' , 
];
