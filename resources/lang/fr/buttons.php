<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Buttons Language Lines
    |--------------------------------------------------------------------------
    |
    |
    */

    'submit' => 'Valider',
    'cancel' => 'Annuler',
    'details' => 'Détails',
    'edit' => 'Modifier',
    'delete' => 'Supprimer',
    'search' => 'Rechercher',

];
