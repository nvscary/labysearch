<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menu Language Lines
    |--------------------------------------------------------------------------
    |
    |
    */

    'login' => 'Se connecter',
    'logout' => 'Se déconnecter',
    'register' => 'Créer un compte',
    'list' => 'Liste',
    'add' => 'Créer',
    'dashboard' => 'Tableau de bord',
    'exams' => 'Analyses',
    'groups' => 'Sous-spécialités',
    'branches' => 'Spécialités',
    'analysis_techniques' => 'Techniques d\'analyses',
    'conditions' => 'Conditions',
    'substances' => 'Substances analysées',

];
