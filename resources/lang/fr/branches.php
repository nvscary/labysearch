<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Branches Language Lines
    |--------------------------------------------------------------------------
    |
    |
    */
    'code' =>'Code' , 
    'title_en' =>'Intitulé de la spécialité - Version Anglaise' , 
    'title_fr' =>'Intitulé de la spécialité - Version Française' , 
    'presentation_en' =>'Présentation/Description - Version Anglaise' ,
    'presentation_fr' =>'Présentation/Description - Version Française' ,
    'create' =>'Enregistrement d\'une nouvelle spécialité' ,
    'list' =>'Liste des spécialités d\'analyse' ,
    'show' =>'Détails de la spécialité' ,
    'edit' =>'Modification d\'une spécialité' ,

    'created' =>'Spécialité sauvegardée !' ,
    'updated' =>'Spécialité mise à jour !' ,
    'deleted' =>'Spécialité supprimée !' ,

    'groups_count' =>'Nombre de sous-spécialités' , 
    'exams_count' =>'Nombre d\'analyses' , 
];
