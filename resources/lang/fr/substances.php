<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Substances Language Lines
    |--------------------------------------------------------------------------
    |
    |
    */
    'code' =>'Code' , 
    'title_en' =>'Intitulé de la substance - Version Anglaise' , 
    'title_fr' =>'Intitulé de la substance - Version Française' , 
    'presentation_en' =>'Présentation/Description - Version Anglaise' ,
    'presentation_fr' =>'Présentation/Description - Version Française' ,
	'create' =>'Enregistrement d\'une nouvelle substance' ,
    'list' =>'Liste des subastances à analyser' ,
    'show' =>'Détails de la substance analysée' ,
    'edit' =>'Modification d\'une substance analysée' ,

    'created' =>'Substance sauvegardée !' ,
    'updated' =>'Substance mise à jour !' ,
    'deleted' =>'Substance supprimée !' ,
];
