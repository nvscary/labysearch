<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Message Language Lines
    |--------------------------------------------------------------------------
    |
    |
    */

    'notfoundrecords' => 'Aucun élément trouvé', 
    'none' => 'Aucun(e)', 
    'choose' => 'Choisir', 
    'search_word' => 'Recherche par mot-clé', 
    'save_error'    => 'Une erreur s\'est produite lors de la sauvegarde. Veuillez vérifier vos infos et rééssayer.',
    'title'    => 'title_fr',
    'presentation'    => 'presentation_fr',
    'cantdelete' =>'Désolé, vous ne pouvez supprimer cet élément !' ,

];
