<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Groups Language Lines
    |--------------------------------------------------------------------------
    |
    |
    */
    'code' =>'Code' , 
    'branch_id' =>'Spécialité' , 
    'title_en' =>'Intitulé de la sous-spécialité - Version Anglaise' , 
    'title_fr' =>'Intitulé de la sous-spécialité - Version Française' , 
    'presentation_en' =>'Présentation/Description - Version Anglaise' ,
    'presentation_fr' =>'Présentation/Description - Version Française' ,
	'create' =>'Enregistrement d\'une nouvelle sous-spécialité' ,
    'list' =>'Liste des sous-spécialités d\'analyse' ,
    'show' =>'Détails de la sous-spécialité' ,
    'edit' =>'Modification d\'une sous-spécialité' ,

    'created' =>'Sous-spécialité sauvegardée !' ,
    'updated' =>'Sous-spécialité mise à jour !' ,
    'deleted' =>'Sous-spécialité supprimée !' ,
    
    'exams_count' =>'Nombre d\'analyses' , 
];
