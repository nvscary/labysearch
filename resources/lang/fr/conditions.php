<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Conditions Language Lines
    |--------------------------------------------------------------------------
    |
    |
    */
    'code' =>'Code' , 
    'title_en' =>'Intitulé de la condition - Version Anglaise' , 
    'title_fr' =>'Intitulé de la condition - Version Française' , 
    'presentation_en' =>'Présentation/Description - Version Française' ,
    'presentation_fr' =>'Présentation/Description - Version Anglaise' ,
	'create' =>'Enregistrement d\'une nouvelle condition d\'analyse' ,
    'list' =>'Liste des conditions d\'analyse' ,
    'show' =>'Détails de la condition' ,
    'edit' =>'Modification d\'une condition' ,

    'created' =>'Condition sauvegardée !' ,
    'updated' =>'Condition mise à jour !' ,
    'deleted' =>'Condition supprimée !' ,
];
