<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Exams Language Lines
    |--------------------------------------------------------------------------
    |
    |
    */

    'code' =>'Code' , 
    'b' =>'B' , 
    'title' =>'Intitulé de l\'analyse' , 
    'title_en' =>'Intitulé de l\'analyse - Version Anglaise' , 
    'title_fr' =>'Intitulé de l\'analyse - Version Française' , 
    'presentation_en' =>'Présentation/Description - Version Anglaise' ,
    'presentation_fr' =>'Présentation/Description - Version Française' ,
    'coast' =>'Coût en FCFA' , 
    'delay' =>'Délais' , 
    'branch_id' =>'Spécialité' , 
    'group_id' =>'Sous-spécialité' , 
    'analysis_technique_id' =>'Technique d\'analyse' , 
    'substance_id' =>'Substance analysée' , 
    'conditions' =>'Conditions d \'examen' , 

    'create' =>'Enregistrement d\'une nouvelle analyse' ,
    'list' =>'List des analyses' ,
    'show' =>'Détails d\'une analyse' ,
    'edit' =>'Editer une analyse' ,

    'created' =>'Analyse sauvegardée !' ,
    'updated' =>'Analyse mise à jour !' ,
    'deleted' =>'Analyse supprimée !' ,
    
    'exams_count' =>'Nombre d\'analyses' , 
];
