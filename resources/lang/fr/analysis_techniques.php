<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Analysis techniques Language Lines
    |--------------------------------------------------------------------------
    |
    |
    */

    'code' =>'Code' , 
    'title_en' =>'Intitulé de la technique - Version Anglaise' , 
    'presentation_en' =>'Présentation / description - Version Anglaise' ,
    'title_fr' =>'Intitulé de la technique - Version Française' , 
    'presentation_fr' =>'Présentation / description - Version Française' ,
    'create' =>'Enregistrer une nouvelle technique' ,
    'list' =>'Liste des techniques d\'analyse' ,
    'show' =>'Détails de la technique d\'analyse' ,
    'edit' =>'Modification d\'une technique d\'analyse' ,

    'created' =>'Technique sauvegardée !' ,
    'updated' =>'Technique mise à jour !' ,
    'deleted' =>'Technique supprimée !' ,
    'cantdelete' =>'Désolé, vous ne pouvez supprimer cet élément !' ,

];
