<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
 
class Condition extends Model  
{ 

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',  'title_en',
         'title_fr'
    ];
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
                  

    /**
     * Lesanalyses
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function exams()
    {
        return $this->hasMany(Exam::class);
    }
                 
}
