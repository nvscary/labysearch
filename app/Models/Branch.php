<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
 

class Branch extends Model 
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'title_en', 'presentation_en',
         'title_fr', 'presentation_fr',
    ];
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
                    

    /**
     * Les sous spécialités
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function groups()
    {
        return $this->hasMany(Group::class);
    }
    /**
     * Les analyses
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function exams()
    {
        return $this->hasMany(Exam::class);
    }
    
    // public function exams()
    // {
    //     return $this->hasManyThrough(Exam::class, Group::class);
    // }             
}
