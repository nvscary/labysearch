<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
 

class Group extends Model  
{  
 /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'title_en', 'presentation_en',
         'title_fr', 'presentation_fr','branch_id'
    ];
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
 
 
    /**
     * La branche
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }
    /**
     * Les analyses
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function exams()
    {
        return $this->hasMany(Exam::class);
    }
                 
}
