<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Exam extends Model
{
   use SearchableTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',  'title_en', 'presentation_en',
         'title_fr', 'presentation_fr',
        'coast', 'delay', 'b','group_id','branch_id',
        'substance_id','analysis_technique_id', 
    ];
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
 
    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'exams.title_fr' => 10,
            'exams.title_en' => 10,
            'exams.code' => 10,
            'exams.presentation_fr' => 2,
            'exams.presentation_en' => 2,
            'branches.title_fr' => 5,
            'branches.title_en' => 5,
            'groups.title_fr' => 2,
            'groups.title_en' => 2,
            'substances.title_fr' => 1,
            'substances.title_en' => 1,
            'analysis_techniques.title_fr' => 1,
            'analysis_techniques.title_en' => 1,
        ],
        'joins' => [
            'groups' => ['exams.group_id','groups.id'],
            'branches' => ['exams.branch_id','branches.id'],
            'substances' => ['exams.substance_id','substances.id'],
            'analysis_techniques' => ['exams.analysis_technique_id','analysis_techniques.id'],
        ],
    ];

    /**
     * Le sous-spécialité
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo(Group::class);
    } 
    /**
     * La spécialité
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function branch()
    {
        return $this->belongsTo(Branch::class);
    } 
    /**
     * La substance analysée
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function substance()
    {
        return $this->belongsTo(Substance::class);
    } 
    /**
     * Le type de rdv
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function appointment_type()
    {
        return $this->belongsTo(AppointmentType::class);
    } 
    /**
     * Les conditions de l'analyse
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function conditions()
    {
        return $this->belongsToMany(Condition::class);
    } 

    /**
     * Les techniques d'analyses
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function analysis_technique()
    {
        return $this->belongsTo(AnalysisTechnique::class);
    } 

}
