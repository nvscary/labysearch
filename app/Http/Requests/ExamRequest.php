<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ExamRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       return $rules = [
            'title_en' => 'bail|required|max:20000|unique:exams',
            'title_fr' => 'bail|required|max:20000|unique:exams',
            'branch_id' => 'required',
            'coast' => 'required',
        ];
    }
}
 