<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App;

class TranslationController extends Controller
{
    /**
     * Change session locale
     * @param  Request $request
     * @return Response
     */
    public function changeLocale(Request $request)
    {
        // $this->validate($request, ['locale' => 'required|in:fr,en']);

        if (Session::has('locale')) {
            $oldlocale = Session::get('locale', \Config::get('app.locale'));
        } else {
            $oldlocale = substr($request->server('HTTP_ACCEPT_LANGUAGE'), 0, 2);
        }

        if ($oldlocale != 'fr' && $oldlocale != 'en') {
            $newlocale = 'en';
        }elseif ($oldlocale == 'fr') {
            $newlocale = 'en';
        }elseif ($oldlocale == 'en') {
           $newlocale = 'fr';
        }
        Session::forget('locale');
        // dd($user->locale);
        App::setLocale($newlocale);
        Session::put('locale', $newlocale);
        // dd(App::getLocale());
        if($request->user())
        {
            $user = $request->user();
            $user->locale = $newlocale;
            $user->save();
        }
        return redirect()->back();
    }
}
