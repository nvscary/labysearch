<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Substance;
use App\Http\Requests\SubstanceRequest;

class SubstanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $substances = Substance::latest()->get();
      
        return view('substances.index', compact('substances'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $substance = new Substance();
      
        return view('substances.create', compact('substance'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubstanceRequest $request)
    {
        try
        {  
            $substance = Substance::create([
                        'code'             => trim($request->input('code')),
                        'title_en'             => $request->input('title_en'),
                        'title_fr'             => $request->input('title_fr'),
                    ]);
        }catch (\Exception $e) { 
            Session::flash('status','error');
            $message = trans('message.save_error')." : ". $e->getMessage();
            Session::flash('message',$message );

            return redirect()->back()->with('error', $message);
        }
        $message =__('substances.created'); 

        Session::flash('success', $message);
        return redirect()->route('substances.index')->with('success', $message);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $substance = Substance::findOrFail($id);
        return view('substances.show', compact('substance'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $substance = Substance::findOrFail($id);
        return view('substances.edit', compact('substance'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SubstanceRequest $request, $id)
    {
        $substance = Substance::findOrFail($id);

        try
        {  
            $substance->update($request->all());
        }catch (\Exception $e) { 
            Session::flash('status','error');
            $message = trans('message.save_error')." : ". $e->getMessage();
            Session::flash('message',$message );

            return redirect()->back()->with('error', $message);
        }
        $message =__('substances.updated'); 

        Session::flash('success', $message);
        return redirect()->route('substances.index')->with('success', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $substance = Substance::findOrFail($id);
 
        if(count($substance->exams)){
            $message = __('message.cantdelete');
            Session::flash('error', $message );
            return redirect()->route('substances.index')->with('error', $message);

        }
         $substance->delete();
 
        $message =__('substances.deleted'); 

        Session::flash('warning', $message);
        return redirect()->route('substances.index')->with('warning', $message);
    }
}
