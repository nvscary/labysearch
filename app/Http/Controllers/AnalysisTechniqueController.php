<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\AnalysisTechnique;
use App\Http\Requests\AnalysisTechniqueRequest;

class AnalysisTechniqueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $analysis_techniques = AnalysisTechnique::latest()->get();
      
        return view('analysis_techniques.index', compact('analysis_techniques'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $analysis_technique = new AnalysisTechnique();
      
        return view('analysis_techniques.create', compact('analysis_technique'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AnalysisTechniqueRequest $request)
    {
        try
        {  
            $analysis_technique = AnalysisTechnique::create([
                        'code'             => trim($request->input('code')),
                        'title_en'             => $request->input('title_en'),
                        'title_fr'             => $request->input('title_fr'),
                        'presentation_en'             => $request->input('presentation_en'),
                        'presentation_fr'             => $request->input('presentation_fr'),
                    ]);
        }catch (\Exception $e) { 
            Session::flash('status','error');
            $message = trans('message.save_error')." : ". $e->getMessage();
            Session::flash('message',$message );

            return redirect()->back()->with('error', $message);
        }
        $message =__('techniques.created'); 

        Session::flash('success', $message);
        return redirect()->route('analysis_techniques.index')->with('success', $message);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $analysis_technique = AnalysisTechnique::findOrFail($id);
        return view('analysis_techniques.show', compact('analysis_technique'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $analysis_technique = AnalysisTechnique::findOrFail($id);
        return view('analysis_techniques.edit', compact('analysis_technique'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AnalysisTechniqueRequest $request, $id)
    {
        $analysis_technique = AnalysisTechnique::findOrFail($id);

        try
        {  
            $analysis_technique->update($request->all());
        }catch (\Exception $e) { 
            Session::flash('status','error');
            $message = trans('message.save_error')." : ". $e->getMessage();
            Session::flash('message',$message );

            return redirect()->back()->with('error', $message);
        }
        $message =__('techniques.updated'); 

        Session::flash('success', $message);
        return redirect()->route('analysis_techniques.index')->with('success', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $analysis_technique = AnalysisTechnique::findOrFail($id);
 
        if(count($analysis_technique->exams)){
    
            $message = __('message.cantdelete');
            Session::flash('error', $message );
            return redirect()->route('analysis_techniques.index')->with('error', $message);

        }
         $analysis_technique->delete();
 
        $message =__('techniques.deleted'); 

        Session::flash('warning', $message);
        return redirect()->route('analysis_techniques.index')->with('warning', $message);
    }
}
