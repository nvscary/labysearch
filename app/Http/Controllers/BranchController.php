<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Branch;
use App\Models\Group;
use App\Http\Requests\BranchRequest;

class BranchController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $branches = Branch::latest()->get();
      
        return view('branches.index', compact('branches'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $branch = new Branch();
      
        return view('branches.create', compact('branch'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BranchRequest $request)
    {
        try
        {  
            $branch = Branch::create([
                        'title_en'             => $request->input('title_en'),
                        'title_fr'             => $request->input('title_fr'),
                        'presentation_en'             => $request->input('presentation_en'),
                        'presentation_fr'             => $request->input('presentation_fr'),
                    ]);
        }catch (\Exception $e) { 
            Session::flash('status','error');
            $message = trans('message.save_error')." : ". $e->getMessage();
            Session::flash('message',$message );

            return redirect()->back()->with('error', $message);
        }
        $message =__('branches.created'); 

        Session::flash('success', $message);
        return redirect()->route('branches.index')->with('success', $message);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $branch = Branch::findOrFail($id);
        return view('branches.show', compact('branch'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $branch = Branch::findOrFail($id);
        return view('branches.edit', compact('branch'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BranchRequest $request, $id)
    {
        $branch = Branch::findOrFail($id);

        try
        {  
            $branch->update($request->all());
        }catch (\Exception $e) { 
            Session::flash('status','error');
            $message = trans('message.save_error')." : ". $e->getMessage();
            Session::flash('message',$message );

            return redirect()->back()->with('error', $message);
        }
        $message =__('branches.updated'); 

        Session::flash('success', $message);
        return redirect()->route('branches.index')->with('success', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $branch = Branch::findOrFail($id);
        // dd(count($branch->exams));
        if(count($branch->groups)|| count($branch->exams)){
            $message = __('message.cantdelete');
            Session::flash('error', $message );
            return redirect()->route('branches.index')->with('error', $message);

        }
         $branch->delete();
 
        $message =__('branches.deleted'); 


        Session::flash('warning', $message);
        return redirect()->route('branches.index')->with('warning', $message);
    }

    /**
     * Rechercher la liste des sous-spécialités d'une spécialité d'une région.
     *
     * @param  Int $branch_id
     * @return \Illuminate\Http\Response
     */
    public function getPluckedGroups(int $branch_id = null)
    {
        if(!isset($branch_id) || is_null($branch_id) )
            return array();

        $branch = Branch::findOrFail($branch_id);
        $groups = Group::where('branch_id', $branch_id)->pluck(trans('message.title'), 'groups.id');
        return $groups;
    }
}
