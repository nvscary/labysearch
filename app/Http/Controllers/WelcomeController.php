<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Exam;

class WelcomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('welcome');
    }
    /**
     * Liste des analyses
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function exams(Request $request)
    {
       
        $exams = Exam::latest()
            ->when ($request->has('group_id'), function ($query) use($request) {
                $query->where('group_id',$request->get('group_id') );
            })->when ($request->has('branch_id'), function ($query) use($request) {
                $query->where('branch_id',$request->get('branch_id') );
            })->when ($request->has('substance_id'), function ($query) use($request) {
                $query->where('substance_id',$request->get('substance_id') );
            })->when ($request->has('analysis_technique_id'), function ($query) use($request) {
                $query->where('analysis_technique_id',$request->get('analysis_technique_id') );
            })
            ->paginate(15);
        
        $links = $exams->links ('front.pagination');
        
        // Ajax response
        if ($request->ajax ()) { 
            return response ()->json ([
                'table' => view ("front.exams.table", ["exams" => $exams])->render (),
                'pagination' => $links->toHtml (),
            ]);
        }

        return view('front.exams.index',compact('exams','links'));
    }

    /**
     *Afficher les détails d'un examen pour le visiteur
     */
    public function exam($id)
    {
        $exam = Exam::findOrFail($id);
        return view('front.exams.show', compact('exam'));
    }

    public function search(Request $request)
    {

        $exams = Exam::with('group')
            ->with('branch')
            ->with('substance')
            ->with('analysis_technique')
            ->when ($request->has('group_id'), function ($query) use($request) {
                $query->where('group_id',$request->get('group_id') );
            })->when ($request->has('branch_id'), function ($query) use($request) {
                $query->where('branch_id',$request->get('branch_id') );
            })->when ($request->has('substance_id'), function ($query) use($request) {
                $query->where('substance_id',$request->get('substance_id') );
            })->when ($request->has('analysis_technique_id'), function ($query) use($request) {
                $query->where('analysis_technique_id',$request->get('analysis_technique_id') );
            })
            ->latest();
        if($request->has('search'))
        {
            $exams = $exams->search($request->get('search'))->get();
        }else  $exams = $exams->get(); 
        
           

        return response ()->json ( $exams);
    }
}
 