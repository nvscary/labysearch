<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Branch;
use App\Models\Group;
use App\Http\Requests\GroupRequest;

class GroupController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $groups = Group::latest()
            ->when ($request->has('group_id'), function ($query) use($request) {
                $query->where('group_id',$request->get('group_id') );
            })->when ($request->has('branch_id'), function ($query) use($request) {
                $query->where('branch_id',$request->get('branch_id') );
            })
            ->get();
        
        return view('groups.index', compact('groups'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $group = new Group();

        if($request->has('branch_id'))
        {
            $group->branch_id = $request->get('branch_id');
        }
        $branches = Branch::orderBy(trans('message.title'))
                            ->pluck(trans('message.title'),'id')
                            ->prepend(trans('message.choose'),'');
        return view('groups.create', compact('group','branches'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GroupRequest $request)
    {
        try
        {  
            $group = Group::create([
                        'code'             => trim($request->input('code')),
                        'title_en'             => $request->input('title_en'),
                        'title_fr'             => $request->input('title_fr'),
                        'branch_id'             => $request->input('branch_id'),
                    ]);
        }catch (\Exception $e) { 
            Session::flash('status','error');
            $message = trans('message.save_error')." : ". $e->getMessage();
            Session::flash('message',$message );

            return redirect()->back()->with('error', $message);
        }
        $message =__('groups.created'); 

        Session::flash('success', $message);
        return redirect()->route('groups.index')->with('success', $message);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $group = Group::findOrFail($id);
        return view('groups.show', compact('group'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $group = Group::findOrFail($id);
        $branches = Branch::orderBy(trans('message.title'))->pluck(trans('message.title'),'id')->prepend(trans('message.choose'),'');
      
        return view('groups.edit', compact('group','branches'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $group = Group::findOrFail($id);

        try
        {  
            $group->update($request->all());
        }catch (\Exception $e) { 
            Session::flash('status','error');
            $message = __('message.save_error');
            Session::flash('message',$message );

            return redirect()->back()->with('error', $message);
        }
        $message =__('groups.updated'); 

        Session::flash('success', $message);
        return redirect()->route('groups.index')->with('success', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $group = Group::findOrFail($id);
 
        if(count($group->exams)){
            $message = __('message.cantdelete');
            Session::flash('error', $message );
            return redirect()->route('groups.index')->with('error', $message);

        }
         $group->delete();
 
        $message =__('groups.deleted'); 


        Session::flash('warning', $message);
        return redirect()->route('groups.index')->with('warning', $message);
    }
}
