<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Condition;
use App\Http\Requests\ConditionRequest;

class ConditionController extends Controller
{
      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $conditions = Condition::latest()->get();
      
        return view('conditions.index', compact('conditions'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $condition = new Condition();
      
        return view('conditions.create', compact('condition'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ConditionRequest $request)
    {
        try
        {  
            $condition = Condition::create([
                        'code'             => trim($request->input('code')),
                        'title_en'             => $request->input('title_en'),
                        'title_fr'             => $request->input('title_fr'),
                    ]);
        }catch (\Exception $e) { 
            Session::flash('status','error');
            $message = trans('message.save_error')." : ". $e->getMessage();
            Session::flash('message',$message );

            return redirect()->back()->with('error', $message);
        }
        $message =__('conditions.created'); 
 
        Session::flash('success', $message);
        return redirect()->route('conditions.index')->with('success', $message);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $condition = Condition::findOrFail($id);
        return view('conditions.show', compact('condition'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $condition = Condition::findOrFail($id);
        return view('conditions.edit', compact('condition'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ConditionRequest $request, $id)
    {
        $condition = Condition::findOrFail($id);

        try
        {  
            $condition->update($request->all());
        }catch (\Exception $e) { 
            Session::flash('status','error');
            
            $message = trans('message.save_error')." : ". $e->getMessage();
            Session::flash('message',$message );

            return redirect()->back()->with('error', $message);
        }
        $message =__('conditions.updated'); 
 
        Session::flash('success', $message);
        return redirect()->route('conditions.index')->with('success', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $condition = Condition::findOrFail($id);
 
        if(count($condition->exams)){
            $message = __('message.cantdelete');
            Session::flash('error', $message );
            return redirect()->route('conditions.index')->with('error', $message);

        }
         $condition->delete();
 
        $message =__('conditions.deleted'); 
 
        Session::flash('warning', $message);
        return redirect()->route('conditions.index')->with('warning', $message);
    }
}
