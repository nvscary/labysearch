<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Exam;
use App\Models\Group;
use App\Models\AnalysisTechnique;
use App\Models\AppointmentType;
use App\Models\Branch;
use App\Models\Substance;
use App\Models\Condition;
use App\Http\Requests\ExamRequest;

class ExamController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $exams = Exam::latest()
            ->when ($request->has('group_id'), function ($query) use($request) {
                $query->where('group_id',$request->get('group_id') );
            })->when ($request->has('branch_id'), function ($query) use($request) {
                $query->where('branch_id',$request->get('branch_id') );
            })->when ($request->has('substance_id'), function ($query) use($request) {
                $query->where('substance_id',$request->get('substance_id') );
            })->when ($request->has('analysis_technique_id'), function ($query) use($request) {
                $query->where('analysis_technique_id',$request->get('analysis_technique_id') );
            })
            ->get();
        
        return view('exams.index', compact('exams'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $exam = new Exam();

        $branches = Branch::orderBy(trans('message.title'))
                            ->pluck(trans('message.title'),'id')
                            ->prepend(trans('message.choose'),'');
        $groups = Group::orderBy(trans('message.title'))
                        ->pluck(trans('message.title'),'id')
                        ->prepend(trans('message.choose'),'');
        $analysis_techniques = AnalysisTechnique::orderBy(trans('message.title'))
                                    ->pluck(trans('message.title'),'id')
                                    ->prepend(trans('message.none'),'');
        $conditions = Condition::orderBy(trans('message.title'))
                            ->pluck(trans('message.title'),'id')
                            ->prepend(trans('message.none'),'');
        $substances = Substance::orderBy(trans('message.title'))
                            ->pluck(trans('message.title'),'id')
                            ->prepend(trans('message.none'),'');
      
        return view('exams.create', 
                    compact('exam','branches','groups','conditions',
                            'analysis_techniques','substances')
                );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ExamRequest $request)
    {
        try
        {  
            $exam = Exam::create([
                        'code'             => trim($request->input('code')),
                        'title_en'             => $request->input('title_en'),
                        'title_fr'             => $request->input('title_fr'),
                        'presentation_en'             => $request->input('presentation_en'),
                        'presentation_fr'             => $request->input('presentation_fr'),
                        'coast'             => $request->input('coast'),
                        'delay'             => $request->input('delay'),
                        'b'             => $request->input('b'),
                        'branch_id'             => $request->input('branch_id'),
                        'group_id'             => $request->input('group_id'),
                        'substance_id'             => $request->input('substance_id'),
                        'analysis_technique_id'             => $request->input('analysis_technique_id'),
                    ]);

        $exam->conditions()->sync($request->input('condition_id'));
        $exam->save();


        }catch (\Exception $e) { 
            Session::flash('status','error');
            $message = trans('message.save_error')." : ". $e->getMessage();
            Session::flash('message',$message );
            dd( $message);
            return redirect()->back()->with('error', $message);
        }
        $message =__('exams.created'); 
 
        Session::flash('success', $message);
        return redirect()->route('exams.index')->with('success', $message);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $exam = Exam::findOrFail($id);
        return view('exams.show', compact('exam'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $exam = Exam::findOrFail($id);
        $branches = Branch::orderBy(trans('message.title'))
                            ->pluck(trans('message.title'),'id')
                            ->prepend(trans('message.choose'),'');
        $groups = Group::orderBy(trans('message.title'))
                        ->pluck(trans('message.title'),'id')
                        ->prepend(trans('message.choose'),'');
        $analysis_techniques = AnalysisTechnique::orderBy(trans('message.title'))
                                    ->pluck(trans('message.title'),'id')
                                    ->prepend(trans('message.none'),'');
        $conditions = Condition::orderBy(trans('message.title'))
                            ->pluck(trans('message.title'),'id')
                            ->prepend(trans('message.none'),'');
        $substances = Substance::orderBy(trans('message.title'))
                            ->pluck(trans('message.title'),'id')
                            ->prepend(trans('message.none'),'');
      
        return view('exams.edit', 
                    compact('exam','branches','groups','conditions',
                            'analysis_techniques','appointment_types','substances')
                );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ExamRequest $request, $id)
    {
        $exam = Exam::findOrFail($id);

        try
        {  
            $exam->update($request->all());
        }catch (\Exception $e) { 
            Session::flash('status','error');
            $message = trans('message.save_error')." : ". $e->getMessage();
            Session::flash('message',$message );

            return redirect()->back()->with('error', $message);
        }
        $message =__('exams.updated');

        Session::flash('success', $message);
        return redirect()->route('exams.index')->with('success', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $exam = Exam::findOrFail($id);

        try {
            $exam->delete();
        } catch (\Exception $e) {
            
            $message = __('message.cantdelete');
            Session::flash('error', $message );
            return redirect()->route('exams.index')->with('error', $message);
        } 
        $message =__('exams.deleted');

        Session::flash('warning', $message);
        return redirect()->route('exams.index')->with('warning', $message);
    }
}
