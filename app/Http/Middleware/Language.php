<?php

namespace App\Http\Middleware;

use Closure;

class Language
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();

        if ($user && $user->locale ) {
            $locale =$user->locale ;
            if ($locale == 'fr' || $locale != 'en') {
                \App::setLocale($locale);
                \Session::put('locale', $locale);
              }

        }else{
            if (\Session::has('locale')) {
                $locale = \Session::get('locale', \Config::get('app.locale'));
            } else {
                $locale = substr($request->server('HTTP_ACCEPT_LANGUAGE'), 0, 2);
            }
            \App::setLocale($locale);
            \Session::put('locale', $locale);
        }
            // dd(\Session::get('locale', \Config::get('app.locale')));
        return $next($request);
    }
}
