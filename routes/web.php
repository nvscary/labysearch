<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::name('changelocale')->get('changelocale', 'TranslationController@changeLocale');

Route::group(['middleware' => 'language'], function () {

	Route::get('/', 'WelcomeController@index')->name('welcome');
	Auth::routes();

	Route::name('front.exams')->get('/front/exams', 'WelcomeController@exams');
	Route::name('front.exam')->get('/front/exams/{id}', 'WelcomeController@exam');
	Route::name('front.search')->post('/front/search', 'WelcomeController@search');
	Route::get('/home', 'HomeController@index')->name('home');

	    //Data Fetch
	Route::name('branches.groups')->get('branches/groups/{branch_id?}', 'BranchController@getPluckedGroups');
	Route::middleware(['auth'])->group(function () {
	    
	    Route::resource('analysis_techniques', 'AnalysisTechniqueController');
	    Route::resource('appointment_types', 'AppointmentTypeController');
	    Route::resource('branches', 'BranchController');
	    Route::resource('groups', 'GroupController');
	    Route::resource('exams', 'ExamController');
	    Route::resource('conditions', 'ConditionController');
	    Route::resource('substances', 'SubstanceController');
	    Route::resource('users', 'UserController');
	    
	});
});
