/*
Navicat MySQL Data Transfer

Source Server         : My Local
Source Server Version : 50719
Source Host           : localhost:3306
Source Database       : labysearch

Target Server Type    : MYSQL
Target Server Version : 50719
File Encoding         : 65001

Date: 2019-02-20 18:22:15
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `analysis_techniques`
-- ----------------------------
DROP TABLE IF EXISTS `analysis_techniques`;
CREATE TABLE `analysis_techniques` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `presentation` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of analysis_techniques
-- ----------------------------
INSERT INTO `analysis_techniques` VALUES ('1', 'NUM', 'Numération', 'La Numération consiste à ...', '2019-02-20 13:26:55', '2019-02-20 13:26:55', null);
INSERT INTO `analysis_techniques` VALUES ('2', 'MIC', 'Microscope', 'Le Microscope consiste à ...', '2019-02-20 13:27:22', '2019-02-20 13:27:30', null);
INSERT INTO `analysis_techniques` VALUES ('3', 'CDF', 'Cyométrie de flux', 'Cyométrie de flux', '2019-02-20 14:00:51', '2019-02-20 14:00:51', null);

-- ----------------------------
-- Table structure for `appointment_types`
-- ----------------------------
DROP TABLE IF EXISTS `appointment_types`;
CREATE TABLE `appointment_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of appointment_types
-- ----------------------------
INSERT INTO `appointment_types` VALUES ('1', 'IMD', 'Immédiat', '2019-02-20 13:28:06', '2019-02-20 13:28:06', null);
INSERT INTO `appointment_types` VALUES ('2', '24', 'Au moins 24 Heures', '2019-02-20 13:28:28', '2019-02-20 13:28:28', null);

-- ----------------------------
-- Table structure for `branches`
-- ----------------------------
DROP TABLE IF EXISTS `branches`;
CREATE TABLE `branches` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `presentation` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of branches
-- ----------------------------
INSERT INTO `branches` VALUES ('1', 'Hématologie / Hematology', 'L\'Hématologie consiste à ...', '2019-02-19 21:49:42', '2019-02-19 21:59:55', null);
INSERT INTO `branches` VALUES ('2', 'Recusandae Accusant', 'Qui anim ea suscipit', '2019-02-19 22:00:10', '2019-02-19 22:00:10', null);

-- ----------------------------
-- Table structure for `exams`
-- ----------------------------
DROP TABLE IF EXISTS `exams`;
CREATE TABLE `exams` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `presentation` text COLLATE utf8mb4_unicode_ci,
  `coast` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delay` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `b` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `group_id` int(10) unsigned NOT NULL,
  `sampling_technique_id` int(10) unsigned DEFAULT NULL,
  `appointment_type_id` int(10) unsigned DEFAULT NULL,
  `sampling_condition_id` int(10) unsigned DEFAULT NULL,
  `analysis_technique_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `branch_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `exams_group_id_foreign` (`group_id`),
  KEY `exams_sampling_technique_id_foreign` (`sampling_technique_id`),
  KEY `exams_appointment_type_id_foreign` (`appointment_type_id`),
  KEY `exams_sampling_condition_id_foreign` (`sampling_condition_id`),
  KEY `exams_analysis_technique_id_foreign` (`analysis_technique_id`),
  KEY `exams_branch_id_foreign` (`branch_id`),
  CONSTRAINT `exams_analysis_technique_id_foreign` FOREIGN KEY (`analysis_technique_id`) REFERENCES `analysis_techniques` (`id`),
  CONSTRAINT `exams_appointment_type_id_foreign` FOREIGN KEY (`appointment_type_id`) REFERENCES `appointment_types` (`id`),
  CONSTRAINT `exams_branch_id_foreign` FOREIGN KEY (`branch_id`) REFERENCES `branches` (`id`),
  CONSTRAINT `exams_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`),
  CONSTRAINT `exams_sampling_condition_id_foreign` FOREIGN KEY (`sampling_condition_id`) REFERENCES `sampling_conditions` (`id`),
  CONSTRAINT `exams_sampling_technique_id_foreign` FOREIGN KEY (`sampling_technique_id`) REFERENCES `sampling_techniques` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of exams
-- ----------------------------
INSERT INTO `exams` VALUES ('1', 'NFS + PL', 'HÉMOGRAMME COMPLET(NFS + PL)', 'Qui anim ea suscipit', '6000', '4 Heures', '30', '1', '3', '1', null, '3', '2019-02-20 13:57:51', '2019-02-20 14:05:33', null, '1');
INSERT INTO `exams` VALUES ('2', 'Omnis corrupti beat', 'Ea in reprehenderit', 'Cupidatat officiis u', 'Aut in delectus vol', 'Debitis similique di', 'Adipisci nisi beatae', '1', '2', '2', '1', '1', '2019-02-20 14:38:22', '2019-02-20 14:38:22', null, '2');

-- ----------------------------
-- Table structure for `groups`
-- ----------------------------
DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `presentation` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `branch_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `groups_branch_id_foreign` (`branch_id`),
  CONSTRAINT `groups_branch_id_foreign` FOREIGN KEY (`branch_id`) REFERENCES `branches` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of groups
-- ----------------------------
INSERT INTO `groups` VALUES ('1', 'Numération Globulaire', null, '2019-02-20 13:25:40', '2019-02-20 13:25:40', null, '1');

-- ----------------------------
-- Table structure for `migrations`
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('1', '2014_10_12_000000_create_users_table', '1');
INSERT INTO `migrations` VALUES ('2', '2014_10_12_100000_create_password_resets_table', '1');
INSERT INTO `migrations` VALUES ('3', '2019_02_19_190841_create_branches_table', '2');
INSERT INTO `migrations` VALUES ('4', '2019_02_19_190947_create_groups_table', '2');
INSERT INTO `migrations` VALUES ('5', '2019_02_19_191113_create_analysis_techniques_table', '2');
INSERT INTO `migrations` VALUES ('6', '2019_02_19_191142_create_sampling_techniques_table', '2');
INSERT INTO `migrations` VALUES ('7', '2019_02_19_191205_create_sampling_conditions_table', '2');
INSERT INTO `migrations` VALUES ('8', '2019_02_19_191233_create_appointment_types_table', '2');
INSERT INTO `migrations` VALUES ('9', '2019_02_19_191751_create_exams_table', '2');
INSERT INTO `migrations` VALUES ('10', '2019_02_20_130331_update_exams_add_branch_id', '3');

-- ----------------------------
-- Table structure for `password_resets`
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for `sampling_conditions`
-- ----------------------------
DROP TABLE IF EXISTS `sampling_conditions`;
CREATE TABLE `sampling_conditions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of sampling_conditions
-- ----------------------------
INSERT INTO `sampling_conditions` VALUES ('1', 'JEUN', 'A jeun', '2019-02-20 13:31:46', '2019-02-20 13:31:46', null);
INSERT INTO `sampling_conditions` VALUES ('2', 'CFR', 'Conservation au frais', '2019-02-20 13:32:37', '2019-02-20 13:32:37', null);

-- ----------------------------
-- Table structure for `sampling_techniques`
-- ----------------------------
DROP TABLE IF EXISTS `sampling_techniques`;
CREATE TABLE `sampling_techniques` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of sampling_techniques
-- ----------------------------
INSERT INTO `sampling_techniques` VALUES ('1', 'TTB', 'Sang total', '2019-02-20 13:34:16', '2019-02-20 13:34:16', null);
INSERT INTO `sampling_techniques` VALUES ('2', 'PSI', 'Ponction sternale ou iliaque', '2019-02-20 13:34:38', '2019-02-20 13:34:38', null);
INSERT INTO `sampling_techniques` VALUES ('3', 'STEDTA', 'Sang total EDTA', '2019-02-20 14:00:03', '2019-02-20 14:00:03', null);

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'Administrator', 'admin@admin.com', null, '$2y$10$kyxNo1rXbUpePqYzk7htje.WdpQhEpEEF/Sw/cpelRVNm8ySuuTzu', '0oimX4Io9z7LFwkrIcUqlolADib1rKRF1RnDjN1Yp6fFyd5R6LVgwMhi4VNz', '2019-02-19 14:49:01', '2019-02-19 14:49:01');
